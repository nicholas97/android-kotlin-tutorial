package com.example.navdemo

import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import com.example.navdemo.databinding.FragmentEmailBinding

class EmailFragment : Fragment() {
    private lateinit var binding: FragmentEmailBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentEmailBinding.inflate(inflater,container,false)
        val inputName = requireArguments().getString("user_name")
        binding.btnSubmit.setOnClickListener {
            if(!TextUtils.isEmpty(binding.etEmail.text.toString())){
                val bundle = bundleOf(
                    "user_name" to inputName,
                    "user_email" to binding.etEmail.text.toString()
                )
                it.findNavController().navigate(R.id.action_emailFragment_to_welcomeFragment,bundle)
            }else{
                Toast.makeText(activity,"Please enter your email",Toast.LENGTH_LONG).show()
            }
        }
        return binding.root
    }


}