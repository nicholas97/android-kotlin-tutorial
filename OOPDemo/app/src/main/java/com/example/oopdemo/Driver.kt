package com.example.oopdemo

import android.util.Log

class Driver(var name:String, credit:Int) {
    private var totalCredit = 50
    private var driverName = name
    val car =Car()

    init{
        totalCredit = totalCredit + credit
        car.maxSpeed = 150
        car.start()
    }
    fun showDetails(){
        Log.i("MYTAG","name of ther driver is $name with credit $totalCredit")
    }
}