package com.example.oopdemo

import android.util.Log

class MyCar :Car(), SpeedController {
    override fun start() {
        super.start()
        Log.i("MyTag","this is MyCar Starting...Brand id is ${getBrandId()}")
    }

    override fun accelerate() {
        TODO("Not yet implemented")
    }

    override fun decelerate() {
        TODO("Not yet implemented")
    }
}