package com.example.viewmodelscopedemo.model

import kotlinx.coroutines.delay

class UserRepository {
    suspend fun getUsers():List<User>{
        delay(8000)
        val users:List<User> = listOf(
            User(1,"Name 1"),
            User(2,"Name 2"),
            User(3,"Name 3"),
            User(4,"Name 4"),
        )
        return users
    }
}